import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {
  menu = false;
  subMenu = {
    delivery: false,
    clients: false
  };
  constructor() { }

  ngOnInit() {
  }

  dropDown(subMenu) {
    if (subMenu === 'delivery') {
      this.subMenu.delivery = !this.subMenu.delivery;
    } else {
      this.subMenu.clients = !this.subMenu.clients;
    }
  }

}
